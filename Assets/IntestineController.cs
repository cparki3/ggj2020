﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntestineController : MonoBehaviour
{

    public PooStationController[] pooStations;
    public bool activated = false;

    public int plungeCount = 0;
    private int plungesNeeded = 4;

    public void activateOrgan()
    {
        activated = true;
        plungeCount = 0;
        foreach(PooStationController station in pooStations)
        {
            station.activatePoo();
        }
    }

    public void plungeComplete()
    {
        plungeCount++;

        if(plungeCount >= plungesNeeded)
        {
            if(BodyManager.instance != null)
            {
                plungeCount = 0;
                activated = false;
                BodyManager.instance.FixOrgan(Organ.Intestines);
            }
        }
    }
}
