﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooStationController : MonoBehaviour
{
    // Start is called before the first frame update
    public float maxY;
    public float minY;

    public GameObject pooBG;

    public Vector3 maxPos;
    public Vector3 minPos;

    private bool canPlunge = false;

    private float plungeDistance = 0;

    public IntestineController intController;

    public int plungesNeeded = 5;
    public int plunges = 0;

    public void Start()
    {
        Vector3 startPos = pooBG.transform.localPosition;
        pooBG.transform.localPosition = new Vector3(startPos.x, minY, startPos.z);
        minPos = pooBG.transform.localPosition;
        maxPos = new Vector3(startPos.x, maxY, startPos.z);

        plungeDistance = (Mathf.Abs(maxY) - Mathf.Abs(minY)) / plungesNeeded;
    }

    public void activatePoo()
    {
        pooBG.transform.DOLocalMoveY(maxY, .5f).OnComplete(() =>
        {
            plunges = 0;
            canPlunge = true;
        });
    }

    public void stationComplete()
    {
        if(intController != null)
        {
            intController.plungeComplete();
        }
    }

    public void lowerPoo()
    {
        if (!canPlunge) { return; }
        canPlunge = false;
        float currentY = pooBG.transform.localPosition.y;
        pooBG.transform.DOLocalMoveY(currentY + plungeDistance, .25f).OnComplete(() =>
        {
            plunges++;
            if (plunges >= plungesNeeded)
            {
                //station complete
                stationComplete();
            }
            else
            {
                canPlunge = true;
            }
        });
        
    }
}
