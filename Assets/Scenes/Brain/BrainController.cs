﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrainController : MonoBehaviour
{
    public Animator brainAnimator;

    private enum Turn { player, computer };

    public int Difficulty = 2;

    List<int> chosenColors = new List<int>();
    Queue<int> colors = new Queue<int>();

    private Turn currentTurn;

    private bool isShowingColors;

    //public Text BrainText;

    // Use this for initialization
    void Start()
    {
        SetColor(5);
        currentTurn = Turn.computer;
    }

    public void RestartGame()
    {
        InitGame();
    }

    private void InitGame()
    {
        currentTurn = Turn.computer;
        chosenColors.Clear();
        colors.Clear();

        isShowingColors = false;

        for (int i = 0; i < Difficulty; i++)
        {
            int randomColor = Random.Range(1, 5);
            chosenColors.Add(randomColor);
        }
        chosenColors.Reverse();
        StartCoroutine(ShowColors());
    }

    private IEnumerator ShowColors()
    {
        isShowingColors = true;
        SetColor(0);
        yield return new WaitForSeconds(1f);
        foreach (var color in chosenColors)
        {
            SetColor(color);
            Debug.Log(color.ToString());
            colors.Enqueue(color);
            yield return new WaitForSeconds(.5f);
            SetColor(0);
            yield return new WaitForSeconds(.5f);
        }


        SetColor(0);
        currentTurn = Turn.player;
        isShowingColors = false;
    }

    public void PressedColor(int index)
    {
        if (!isShowingColors && currentTurn == Turn.player)
        {
            var expectedColor = colors.Dequeue();
            SetColor(expectedColor, .5f);
            
            if (expectedColor == index)
            {
                if (colors.Count == 0)
                {
                    //Win condition
                    Invoke("WinCondition", 1f);
                }
            }
            else
            {
                //losse condition
                Invoke("LoseCondition", 1f);
            }
        }
    }

    private void WinCondition()
    {
        currentTurn = Turn.computer;
        SetColor(5);

        BodyManager.instance.FixOrgan(Organ.Brain);
    }

    private void LoseCondition()
    {
        currentTurn = Turn.computer;
        SetColor(-1);
    }

    private void SetColor(int color, float duration = 0f)
    {
        CancelInvoke("ClearColor");
        brainAnimator.SetInteger("ColorState", color);
        if(duration > 0f)
        {
            Invoke("ClearColor", duration);
        }
    }

    private void ClearColor()
    {
        brainAnimator.SetInteger("ColorState", 0);
    }

    public void activateOrgan()
    {
        SetColor(-1);
    }

}
