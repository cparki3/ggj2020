﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class HitBrainButton : MonoBehaviour
{
    private bool CanHit = true;
    public UnityEvent OnHit;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CanHit && collision.gameObject.tag == "Player")
        {
            //animator.SetBool("Hit", true);
            OnHit.Invoke();
            CanHit = false;
            Invoke("ResetHit", 1f);
        }
    }

    private void ResetHit()
    {
        CanHit = true;
    }
}
