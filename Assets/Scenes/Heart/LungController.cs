﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class LungController : MonoBehaviour
{
    private Animator _animator;
    public float Effectiveness = 5f;
    public LungController OtherLung;
    public bool StartingState = true;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.SetBool("Inflate", StartingState);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !_animator.GetBool("Inflate"))
        {
            _animator.SetBool("Inflate", true);
            HeartController.UpdateHeartLevel(Effectiveness);
            OtherLung.MirrorLung(false);
        }
    }

    public void MirrorLung(bool inflation)
    {
        _animator.SetBool("Inflate", inflation);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
