﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateHeartText : MonoBehaviour
{
    private Text heartText;
    // Start is called before the first frame update
    void Start()
    {
        heartText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.H))
        {
            HeartController.SetHeartLevel(50f);
        }

        heartText.text = HeartController.HeartPercent.ToString();
    }
}
