﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    public Animator heartAnimator;
    public static HeartController _s;

    public float HeartLevel = 100f;
    public float MaxHeartLevel = 100f;

    private bool _isBroken = false;
    public bool isBroken {
        get {
            return _isBroken;
        }
        set
        {
            _isBroken = value;
            heartAnimator.SetBool("FastBeat", value);
        }
    }

    public static float HeartPercent
    {
        get
        {
            return _s.HeartLevel/_s.MaxHeartLevel;
        }
    }
    // Start is called before the first frame update
    private void Awake()
    {
        if (_s != null)
        {
            Destroy(this);
        }
        else
        {
            _s = this;
        }
    }

    void Start()
    {
        HeartLevel = MaxHeartLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isBroken && HeartLevel == MaxHeartLevel)
        {
            isBroken = false;
            BodyManager.instance.FixOrgan(Organ.Heart);
        }
    }

    public static void SetHeartLevel(float amt)
    {
        _s.HeartLevel = amt;
    }

    public static void UpdateHeartLevel(float amt)
    {
        _s.HeartLevel = _s.HeartLevel + amt > _s.MaxHeartLevel ? _s.MaxHeartLevel : _s.HeartLevel + amt;
    }

    public void activateOrgan()
    {
        SetHeartLevel(MaxHeartLevel / 4);
        isBroken = true;
    }

}
