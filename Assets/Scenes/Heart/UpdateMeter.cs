﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMeter : MonoBehaviour
{
    public SpriteRenderer meterSprite;
    private int maxYScale = 4;
    
    // Start is called before the first frame update
    void Awake()
    {
    }   

    // Update is called once per frame
    void Update()
    {
        var scale = transform.localScale;
        scale.y = maxYScale * HeartController.HeartPercent;
        transform.localScale = scale;
    }
}
