﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiverController : MonoBehaviour
{
    public int nutrientsNeeded = 10;
    public int nutrientsGathered = 0;

    public Timer[] spawnTimers;

    public Spawner[] spawners;

    public bool organActive = false;

    public void Start()
    {
    
    }

    public void activateOrgan()
    {
        if (!organActive) {
            nutrientsGathered = 0;
            startDispensing();
            organActive = true;
        }
    }

    public void toxinAdded()
    {
        if(nutrientsGathered > 0)
        {
            nutrientsGathered--;
        }
    }

    public void nutrientAdded()
    {
        nutrientsGathered++;
        if(nutrientsGathered >= nutrientsNeeded)
        {
            if(BodyManager.instance != null)
            {
                organActive = false;
                BodyManager.instance.FixOrgan(Organ.Liver);
                //stopDispensing();
            }
        }
    }

    public void itemDestroyed(int dispenserIndex)
    {
        if (nutrientsGathered < nutrientsNeeded)
        {
            spawners[dispenserIndex].Spawn();
        }
    }

    public void startDispensing()
    {
        /*foreach(Timer tmr in spawnTimers)
        {
            tmr.pauseTimer = false;
        }*/

        foreach(Spawner spwn in spawners)
        {
            spwn.Spawn();
        }
    }

    private void stopDispensing()
    {
        /*foreach (Timer tmr in spawnTimers)
        {
            tmr.pauseTimer = true;
        }*/
    }
}
