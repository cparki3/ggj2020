﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroinManager : MonoBehaviour
{
    public GameObject[] buttons;
    private int pressCount = 0;

    private void Start()
    {
        deactivateButtons();
    }

    public void activateOrgan()
    {
        pressCount = 0;

        activateButtons();
    }


    public void deactivateButtons()
    {
        foreach (GameObject button in buttons)
        {
            button.SetActive(false);
        }
    }

    public void activateButtons()
    {
        foreach(GameObject button in buttons)
        {
            button.SetActive(true);
        }
    }

    public void switchFlipped()
    {
        pressCount++;
        if(pressCount >= buttons.Length)
        {
            if(BodyManager.instance != null)
            {
                BodyManager.instance.FixOrgan(Organ.Groin);
            }
        }
    }
}
