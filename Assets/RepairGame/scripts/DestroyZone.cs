﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class DestroyZone : MonoBehaviour
{

    public UnityEvent ObjectDestroyed = new UnityEvent();

    public LiverController lc;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            return;
        }
        if(lc != null)
        {
            if (collision.gameObject.CompareTag("Toxin"))
            {
                lc.toxinAdded();
            }
            if (collision.gameObject.CompareTag("Nutrient"))
            {
                lc.nutrientAdded();
            }
        }
        Rigidbody2D rb = collision.attachedRigidbody;
        if (rb == null)
            return;

        Destroy(rb.gameObject);
        ObjectDestroyed.Invoke();
    }
}
