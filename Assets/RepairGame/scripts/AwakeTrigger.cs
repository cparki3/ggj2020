﻿using UnityEngine;
using UnityEngine.Events;

public class AwakeTrigger : MonoBehaviour
{
    public UnityEvent Awaking = new UnityEvent();
    private void Awake() => Awaking.Invoke();
}
