﻿using MoreMountains.CorgiEngine;
using UnityEngine;

public class LeverController : MonoBehaviour
{
    public KeyCode ToggleKeyCode;
    public ButtonActivated LeverButton;
    private void Update()
    {
        if (Input.GetKeyDown(ToggleKeyCode))
            LeverButton.TriggerButtonAction();
    }

}
