﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] Prefabs;
    public Transform SpawnParent;
    public void Spawn()
    {
        if (Prefabs.Length == 0)
            return;

        int p = Random.Range(0, Prefabs.Length);
        Instantiate(Prefabs[p], transform.position, transform.rotation, SpawnParent);
    }
}
