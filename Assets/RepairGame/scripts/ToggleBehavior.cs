﻿using UnityEngine;
using UnityEngine.Events;

public class ToggleBehavior : MonoBehaviour
{
    private bool _state = true;

    public UnityEvent BecameTrue = new UnityEvent();
    public UnityEvent BecameFalse = new UnityEvent();

    public void Toggle()
    {
        _state = !_state;
        (_state ? BecameTrue : BecameFalse).Invoke();
    }
}
