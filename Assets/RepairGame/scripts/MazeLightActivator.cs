﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class MazeLightActivator : MonoBehaviour
{

    public Sprite OnSprite;
    public SpriteRenderer[] Lights;

    public void ActivateLights(int count)
    {
        Assert.IsTrue(count <= Lights.Length, $"Cannot activate {count} lights, there are only {Lights.Length}");

        IList<int> indices = Enumerable.Range(0, Lights.Length).ToList();
        for (int s = 0; s < count; ++s)
        {
            int randIndex = Random.Range(0, indices.Count);
            SpriteRenderer randLight = Lights[indices[randIndex]];
            indices.RemoveAt(randIndex);

            randLight.sprite = OnSprite;
        }
    }
}
