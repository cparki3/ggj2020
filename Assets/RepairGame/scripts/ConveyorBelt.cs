﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ConveyorBelt : MonoBehaviour
{
    private HashSet<Rigidbody2D> _affectedBodies = new HashSet<Rigidbody2D>();


    public float Speed = 2f;

    public void ReverseSpeed()
    {
        Speed = Speed * -1;
    }

    public void StartSpeed()
    {
        Speed = Mathf.Abs(Speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Rigidbody2D rb = collision.attachedRigidbody;
        if (rb != null)
            _affectedBodies.Add(rb);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Rigidbody2D rb = collision.attachedRigidbody;
        if (rb != null && _affectedBodies.Contains(rb))
            _affectedBodies.Remove(rb);
    }
    private void FixedUpdate()
    {
        Vector2 delta = Speed * transform.right * Time.fixedDeltaTime;
        foreach (Rigidbody2D rb in _affectedBodies)
        {
            rb.MovePosition(rb.position + delta);
        }
    }
}
