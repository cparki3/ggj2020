﻿using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    private float _tElapsed = 0f;
    private float _currDuration = -1f;
    public bool pauseTimer = true;

    [Min(0f)]
    public float MinTime = 0.5f;
    public float MaxTime = 2f;
    public UnityEvent Elapsed = new UnityEvent();
    private void Update()
    {
        if (pauseTimer) { return; }
        if (_tElapsed < _currDuration)
        {
            _tElapsed += Time.deltaTime;
            return;
        }

        Elapsed.Invoke();
        _tElapsed = 0f;
        _currDuration = Random.Range(MinTime, MaxTime);
    }
}
