﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformable : MonoBehaviour
{
    public Transform Target;

    public void SetLocalRotationX(float degrees) {
        Vector3 eulerAngles = Target.localRotation.eulerAngles;
        eulerAngles.x = degrees;
        Target.localRotation = Quaternion.Euler(eulerAngles);
    }
    public void SetLocalRotationY(float degrees)
    {
        Vector3 eulerAngles = Target.localRotation.eulerAngles;
        eulerAngles.y = degrees;
        Target.localRotation = Quaternion.Euler(eulerAngles);
    }
    public void SetLocalRotationZ(float degrees)
    {
        Vector3 eulerAngles = Target.localRotation.eulerAngles;
        eulerAngles.z = degrees;
        Target.localRotation = Quaternion.Euler(eulerAngles);
    }
    public void SetRotationX(float degrees)
    {
        Vector3 eulerAngles = Target.rotation.eulerAngles;
        eulerAngles.x = degrees;
        Target.rotation = Quaternion.Euler(eulerAngles);
    }
    public void SetRotationY(float degrees)
    {
        Vector3 eulerAngles = Target.rotation.eulerAngles;
        eulerAngles.y = degrees;
        Target.rotation = Quaternion.Euler(eulerAngles);
    }
    public void SetRotationZ(float degrees)
    {
        Vector3 eulerAngles = Target.rotation.eulerAngles;
        eulerAngles.z = degrees;
        Target.rotation = Quaternion.Euler(eulerAngles);
    }
}
