﻿using UnityEngine;

public class BodyArea : MonoBehaviour
{
    public SpriteRenderer alertRenderer;
    public AudioSource playerPresentAudio;
    public AudioSource alertAudio;

    public int alertLevel = 0;
    public int lastAlertLevel = 0;
    public float timeToRespond = 60f;

    public BodyPartData Data;
    public GameObject organObject;
}
