﻿using UnityEngine;
using UnityEngine.Assertions;

public partial class BodyManager : MonoBehaviour
{
    public static BodyManager instance;


    private bool isDead = false;
    private int _failRandomCount = 0;

    public GameObject gameOverScreen;

    public float degradeSpeed = 3f;

    [Header("Body Part Room Managers (COLORS / TIME)")]
    public BodyArea[] BodyAreas;

    void Awake()
    {
        instance = this;

        foreach (BodyArea bodyArea in BodyAreas)
            repairBodyArea(bodyArea);
    }

    private void repairBodyArea(BodyArea areaToFix)
    {
        areaToFix.timeToRespond = areaToFix.Data.maxTime;
        areaToFix.alertLevel = 0;
    }

    private void startBodyAlert()
    {
        int bodyAlertCount = 0;
        for (int i = 0; i < BodyAreas.Length; i++)
        {
            if (BodyAreas[i].alertLevel > 0)
            {
                bodyAlertCount++;
            }
        }

        Assert.IsFalse(bodyAlertCount >= BodyAreas.Length, "TOO MANY ALERTS HAPPENING RIGHT NOW!");

        if (BodyAreas.Length == 0)
            return;

        int randAreaIndex = Random.Range(0, BodyAreas.Length);
        BodyArea randArea = BodyAreas[randAreaIndex];
        if (randArea.alertLevel > 0)
        {
            if (_failRandomCount > 3)
            {
                _failRandomCount = 0;
                for (int i = 0; i < BodyAreas.Length; i++)
                {
                    if (BodyAreas[i].alertLevel == 0)
                    {
                        BodyAreas[i].alertLevel = 1;
                        BodyAreas[i].organObject.SendMessage("activateOrgan", SendMessageOptions.DontRequireReceiver);
                        return;
                    }
                }
            }
            startBodyAlert();
            _failRandomCount += 1;
        }
        else
        {
            _failRandomCount = 0;
            randArea.alertLevel = 1;
            randArea.organObject.SendMessage("activateOrgan", SendMessageOptions.DontRequireReceiver);
        }
    }

    void Update()
    {
        if (isDead)
            return;

        if (Input.GetKeyUp(KeyCode.V))
        {
            startBodyAlert();
        }
        checkOrganLoop();
        checkAreaLoop();
    }

    private void checkAreaLoop()
    {
        foreach (BodyArea bodyArea in BodyAreas)
            checkAlertLevel(bodyArea, bodyArea.alertLevel);
    }

    private void checkOrganLoop()
    {
        foreach (BodyArea bodyArea in BodyAreas)
        {
            if (bodyArea.alertLevel == 0)
                continue;

            bodyArea.timeToRespond -= degradeSpeed * Time.deltaTime;
            if (bodyArea.alertLevel != 2 && bodyArea.timeToRespond < bodyArea.Data.maxTime / 2)
            {
                bodyArea.alertLevel = 2;
            }
            if (bodyArea.timeToRespond <= 0)
            {
                handleDeath();
            }
        }
    }

    public void handleDeath()
    {
        isDead = true;
        gameOverScreen.SetActive(true);
    }

    public void FixOrgan(Organ organ)
    {
        foreach (BodyArea bodyArea in BodyAreas)
        {
            if (bodyArea.Data.organ == organ)
            {
                repairBodyArea(bodyArea);
            }
        }
    }
    private void checkAlertLevel(BodyArea bodyArea, int alertLevel)
    {
        if (alertLevel == bodyArea.lastAlertLevel)
            return;

        bodyArea.lastAlertLevel = alertLevel;

        bodyArea.alertAudio.volume = (alertLevel == 0) ? 0f : 1f;

        switch (alertLevel)
        {
            case 0: bodyArea.alertRenderer.color = bodyArea.Data.alertZeroColor; break;
            case 1: bodyArea.alertRenderer.color = bodyArea.Data.alertOneColor; break;
            case 2: bodyArea.alertRenderer.color = bodyArea.Data.alertTwoColor; break;
            default: break;
        }
    }

}
