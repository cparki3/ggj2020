﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePoo : MonoBehaviour
{

    public float StartSpeed = 2f;
    public float AdjustSpeed = 0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        pos.y += (StartSpeed - AdjustSpeed) * Time.deltaTime / 50f;
        transform.position = pos;

        AdjustSpeed = 0f;
    }

    public void SetAdjustSpeed(float value)
    {
        AdjustSpeed = value;
    }

}
