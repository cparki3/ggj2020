﻿public enum Organ
{ 
    Brain,
    Heart,
    Liver,
    Intestines,
    Groin
};
