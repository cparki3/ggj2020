﻿using UnityEngine;

public class OrganButtonSwitchTester : MonoBehaviour
{
    public Organ Organ;

    public void Reset() => BodyManager.instance.FixOrgan(Organ);
}
