﻿using MoreMountains.CorgiEngine;
using UnityEngine;

public class BodyAreaMusicTrigger : MonoBehaviour
{
    private int _playerCount = 0;

    public BodyArea BodyArea;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.attachedRigidbody.TryGetComponent<Character>(out _) && ++_playerCount == 1)
            BodyArea.playerPresentAudio.volume = 1f;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.attachedRigidbody.TryGetComponent<Character>(out _) && --_playerCount == 0)
            BodyArea.playerPresentAudio.volume = 0f;
    }
}
