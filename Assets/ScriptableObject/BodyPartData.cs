﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/" + nameof(BodyPartData), order = 1)]
public class BodyPartData : ScriptableObject
{
    public float maxTime = 60f;
    public string[] alertMessages;
    public Organ organ;

    public Color alertZeroColor = Color.white;
    public Color alertOneColor = Color.yellow;
    public Color alertTwoColor = Color.red;
}
